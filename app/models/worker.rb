class Worker < ActiveRecord::Base
  validates :name, 
    presence: true, 
    uniqueness: true, 
    format: { with: /\A[a-z0-9]+\z/ }
  
  has_many :task

  # Si en lugar de detener el grabado de datos queremos controlarlos para acomodarlos a nuestas
  #   especificaciones, podemos realizar transformaciones de los datos antes de la validación 
  # before_validation :name_to_downcase

  # private

  # def name_to_downcase
  #   self.name.downcase!
  # end
end
