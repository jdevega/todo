class Task < ActiveRecord::Base

  # Esto significa que una instancia de Task solo puede estar relacionada con una instancia de Worker y al revés
  # has_one :worker

  # Esta relación permite que varias instancias de Task tengan asociada la misma instancia de Worker
  belongs_to :worker

  # Validaciones
  # Más info : http://guides.rubyonrails.org/active_record_validations.html
  validates :name, 
    presence: true, 
    uniqueness: true, 
    length: { in: 6..20 }
  validates :worker_id, 
    presence: { message: "Debes asociar un trabajador a la tarea" }
  #validate :worker_exists, on: [:create, :update]

  def worker_name; worker && worker.name; end
  def done!; self.update(done: true); end
  def pending!; self.update(done: false); end
  def status; done? ? 'done' : 'pending'; end

  private

  def worker_exists
    errors.add(:worker_id, 'no existe') unless Worker.exists?(self.worker_id)
  end
end
