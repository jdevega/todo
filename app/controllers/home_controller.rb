class HomeController < ApplicationController

  # No hace falta definir las acciones si no se van a pasar objetos a la vista. Rails infiere que vista tiene que pintar
  #   a partir del nombre de la acción.
  # Siempre es obligatorio definir la vista asociada a la acción.

  # def index;;end

  # def index
  #   @user_name = 'Usuario'
  # end
end