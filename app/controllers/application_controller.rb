class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # # Opción 1. Usar before_filter con un bloque
  # before_filter do
  #   @app_title = 'Redradix ToDo'
  # end

  # # Opción 2. Usar una función privada en el before_filter
  # before_filter :set_app_title

  # before_action es un nuevo alias para before_filter. A partir de Rails 4 before_action es el estándar.
  before_action :menu

  def menu
    #
    # Cargar los modelos
    #
    eager_load_models
    
    #
    # Recuperamos las clases de los nombres de tablas de ActiveRecord::Base
    #
    @menu_options = ActiveRecord::Base.connection.tables.map do |table|

      # name      = table
      # constant  = name.constantize
      # 
      # Sintaxis para definir varias variables en una sóla línea
      #   En este modo no podemos hacer referencia a variables en la parte izquierda desde la parte
      #   derecha de la asignación : 
      #
      #   name, constant = table, name.constantize
      #
      #   Esto produce el error undefined method 'constantize' for nil:NilClass por que name no está establecido
      #
      name, constant = table, ( table.classify.constantize rescue nil )
      
      # unless name == 'schema_migrations'
      #   {name: name.capitalize, url: "/#{name.pluralize}"}
      # end

      #
      # En Ruby las condiciones pueden ir al final de la línea para mayor claridad
      #
      {name: name.capitalize, url: "/#{name.pluralize}"} unless name == 'schema_migrations'
    end
    .compact # compact elimina los elementos con valor nil del array
  end

  private

  # def set_app_title
  #   @app_title = 'Redradix ToDo'
  # end 

  def eager_load_models
    #
    # Sólamente necesitamos cargar los modelos que se encuentran en la carpeta models que son nuestros modelos de negocio
    #
    # Esto es necesario si la aplicación sólo carga los modelos cuando van a ser usados, lo cual se
    #   indica en configuración con el parámetro cache_classes que por defecto está a false en 
    #   el entorno de development.
    # Mas info en : http://guides.rubyonrails.org/configuring.html ( Sección 3.1 )
    #
    Rails.application.paths["app/models"].eager_load! if Rails.application.config.cache_classes
  end

end
