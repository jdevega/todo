class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy, :done, :pending]
  # Cargar los workers que se van a mostrar en el desplegable del formulario
  before_action :set_workers, only: [:new, :edit, :update]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to :root, notice: 'Task was successfully created.' }
        #
        # ¿ Por qué no renderizamos el index y listo ?
        #
        # format.html { render :index, notice: 'Task was successfully created.' }
        #
        # Por que render simplemente lo que hace es mostrar la plantilla que le indicamos usando las
        #   variables que se han cargado hasta este momento en la acción en curso. 
        # Si rendeamos :index, @tasks no está establecido por que establece cuando ejecutamos 
        #   la acción :index ( línea 9 ). 
        
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to root_url, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /tasks/1/done
  # POST /tasks/1/done.json
  def done
    # @task = Task.find(params[:id])
    # Esto lo podemos hacer incluyendo la accion :done en before_action :set_task
    @task.done!
    respond_to do |format|
      format.html { redirect_to :tasks, notice: "Task #{@task.name} is done!" }
      format.json { render :show, status: :ok, location: @task }
    end
  end

  # POST /tasks/1/pending
  # POST /tasks/1/pending.json
  def pending
    # @task = Task.find(params[:id])
    # Esto lo podemos hacer incluyendo la accion :pending en before_action :set_task
    @task.pending!
    respond_to do |format|
      format.html { redirect_to :tasks, notice: "Task #{@task.name} is not done yet!" }
      format.json { render :show, status: :ok, location: @task }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      # Añadimos el parámetro worker_id a los parámetros permitidos
      params.require(:task).permit(:name, :done, :worker_id)
    end

    def set_workers
      @workers  = Worker.all.map{|w| [w.name, w.id]}
    end
end
