class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.boolean :done

      t.timestamps
    end
    add_index :tasks, :done
  end
end
